import base64
import boto3
import gzip
import os
import requests
import shutil

from requests.auth import HTTPBasicAuth


def get_secret(secret_name):
    region_name = "us-east-1"
    session = boto3.session.Session()
    client = session.client(
        service_name="secretsmanager", region_name=region_name
    )
    get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    if "SecretString" in get_secret_value_response:
        return get_secret_value_response["SecretString"]
    else:
        return base64.b64decode(get_secret_value_response["SecretBinary"])


BUCKET = get_secret("bucket-name-outputs-private")
LOGS_URL = "https://loganalysis.handle.net/crossref/"


def get_credentials():
    client = boto3.client("s3")
    return (
        client.get_object(Bucket=BUCKET, Key="resolution-logs/credentials")[
            "Body"
        ]
        .read()
        .decode("utf-8")
        .strip()
        .split(":")
    )


def summary_process(file_info):
    client = boto3.client("s3")
    credentials = get_credentials()
    auth = HTTPBasicAuth(*credentials)
    summary = requests.get(f"{LOGS_URL}logs/config.json", auth=auth).json()

    date = f"{summary['date'][:4]}-{summary['date'][-2:]}"

    path = file_info[1]

    if not path.endswith(".gz"):
        return

    gz_file = path[5:]
    txt_file = gz_file[:-3]
    expected_lines = int(file_info[2])

    print("downloading", path)
    with requests.get(f"{LOGS_URL}{path}", auth=auth, stream=True) as r:
        with open(gz_file, "wb") as f:
            shutil.copyfileobj(r.raw, f)

    print("unzipping", path)
    with gzip.open(gz_file, "rb") as f_in:
        with open(txt_file, "wb") as f_out:
            shutil.copyfileobj(f_in, f_out)

    lines = 0
    with open(txt_file, "r") as f:
        lines = sum(1 for _ in f)
    if lines != expected_lines:
        raise Exception(
            f"Incorrect number of lines: expected {expected_lines}, "
            f"got {lines}"
        )
    print("lines verified successfully")

    print("uploading to S3", path)
    # client.upload_file(txt_file, BUCKET, f"resolution-logs/{date}/raw/{txt_file}")

    print("removing local files")
    os.remove(gz_file)
    os.remove(txt_file)


def main_function():
    client = boto3.client("s3")
    credentials = get_credentials()
    auth = HTTPBasicAuth(*credentials)
    summary = requests.get(f"{LOGS_URL}logs/config.json", auth=auth).json()

    from distrunner.distrunner import DistRunner

    requirements = ["distrunner", "boto3", "requests", "claws"]

    with DistRunner(
        workers=len(summary["files"]),
        worker_disk_size=200,
        worker_memory="16Gb",
        requirements=requirements,
        application_name="unpack_resolution",
    ) as cldr:
        results = cldr.client.map(summary_process, summary["files"])
        cldr.client.gather(results)


if __name__ == "__main__":
    main_function()
