import base64
from datetime import datetime
from datetime import timedelta

import boto3
from airflow.decorators import dag, task


# NB BUCKET and LOGS_URL constants are duplicated. Once for airflow and once
# for Dask


DEFAULT_ARGS = {
    "owner": "meve",
    "depends_on_past": False,
    "email": "labs@crossref.org",
    "email_on_failure": False,
    "email_on_retry": False,
}

REQUIREMENTS = [
    "boto3",
    "urllib3==1.26.9",
    "claws==0.0.20",
    "smart_open",
    "clannotation==0.0.7",
    "coiled",
    "dask[complete]",
    "longsight>=1.0.13",
    "distrunner",
]


@dag(
    default_args=DEFAULT_ARGS,
    schedule_interval="@daily",
    catchup=False,
    dagrun_timeout=timedelta(hours=16),
    start_date=datetime(2023, 4, 16),
    tags=["reports"],
)
def unpack():
    @task.virtualenv(
        task_id="unpack_reports",
        requirements=REQUIREMENTS,
        system_site_packages=True,
    )
    def unpack_reports(requirements):
        from longsight import instrumentation as ls

        @ls.instrument(
            create_aws=True,
            bucket="outputs.research.crossref.org",
            cloudwatch_push=True,
            namespace="labs",
            log_stream_name="labs-airflow-generate-all",
            log_group_name="labs-airflow-periodic-jobs",
        )
        def unpack_reports_body(file_info, instrumentation=None):
            def get_secret(secret_name):
                region_name = "us-east-1"
                session = boto3.session.Session()
                client = session.client(
                    service_name="secretsmanager", region_name=region_name
                )
                get_secret_value_response = client.get_secret_value(
                    SecretId=secret_name
                )
                if "SecretString" in get_secret_value_response:
                    return get_secret_value_response["SecretString"]
                else:
                    return base64.b64decode(
                        get_secret_value_response["SecretBinary"]
                    )

            import boto3
            from requests.auth import HTTPBasicAuth
            import gzip
            import os
            import shutil

            BUCKET = get_secret("bucket-name-outputs-private")
            LOGS_URL = "https://loganalysis.handle.net/crossref/"

            session = boto3.session.Session()
            client = session.client("s3")

            credentials = (
                client.get_object(
                    Bucket=BUCKET, Key="resolution-logs/credentials"
                )["Body"]
                .read()
                .decode("utf-8")
                .strip()
                .split(":")
            )
            auth = HTTPBasicAuth(*credentials)
            summary = requests.get(
                f"{LOGS_URL}logs/config.json", auth=auth
            ).json()

            date = f"{summary['date'][:4]}-{summary['date'][-2:]}"

            path = file_info[1]

            if not path.endswith(".gz"):
                return

            gz_file = path[5:]
            txt_file = gz_file[:-3]
            expected_lines = int(file_info[2])

            print("downloading", path)
            with requests.get(
                f"{LOGS_URL}{path}",
                auth=auth,
                stream=True,
            ) as r:
                with open(gz_file, "wb") as f:
                    shutil.copyfileobj(r.raw, f)

            print("unzipping", path)
            with gzip.open(gz_file, "rb") as f_in:
                with open(txt_file, "wb") as f_out:
                    shutil.copyfileobj(f_in, f_out)

            lines = 0
            with open(txt_file, "r") as f:
                lines = sum(1 for _ in f)
            if lines != expected_lines:
                raise Exception(
                    f"Incorrect number of lines: expected {expected_lines}, "
                    f"got {lines}"
                )
            print("lines verified successfully")

            print("uploading to S3", path)
            # client.upload_file(txt_file, BUCKET, f"resolution-logs/{date}/raw/{txt_file}")

            print("removing local files")
            os.remove(gz_file)
            os.remove(txt_file)

        from distrunner.distrunner import DistRunner
        import boto3
        from requests.auth import HTTPBasicAuth
        import requests

        def get_secret(secret_name):
            region_name = "us-east-1"
            session = boto3.session.Session()
            client = session.client(
                service_name="secretsmanager", region_name=region_name
            )
            get_secret_value_response = client.get_secret_value(
                SecretId=secret_name
            )
            if "SecretString" in get_secret_value_response:
                return get_secret_value_response["SecretString"]
            else:
                return base64.b64decode(
                    get_secret_value_response["SecretBinary"]
                )

        BUCKET = get_secret("bucket-name-outputs-private")
        LOGS_URL = "https://loganalysis.handle.net/crossref/"

        client = boto3.client("s3")
        credentials = (
            client.get_object(Bucket=BUCKET, Key="resolution-logs/credentials")[
                "Body"
            ]
            .read()
            .decode("utf-8")
            .strip()
            .split(":")
        )
        auth = HTTPBasicAuth(*credentials)

        summary = requests.get(
            f"{LOGS_URL}logs/config.json",
            auth=auth,
        ).json()

        with DistRunner(
            workers=len(summary["files"]),
            worker_disk_size=200,
            worker_memory="16Gb",
            requirements=requirements,
            application_name="unpack_resolution",
        ) as cldr:
            results = cldr.client.map(unpack_reports_body, summary["files"])
            cldr.client.gather(results)

    unpack_reports(REQUIREMENTS)


unpack()
